using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speedup : PowerBase
{
    // Start is called before the first frame update
    void Start()
    {
        Timee = Contants.TIME_SPEEDUP;    
    }
    protected override void PowEffect(HitPowerup playerhit)
    {
        base.PowEffect(playerhit);
        Debug.Log("speedup");
        playerhit.SpeedUp(Timee);
    }
}
