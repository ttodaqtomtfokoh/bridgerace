using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSpawn : MonoBehaviour
{
    public GameObject[] PowerUp;
    private float SpawnRange;
    private float[] SpawnTime = { 0, 0 };
    [HideInInspector] public List<GameObject> power_spawned = new List<GameObject>();
    [HideInInspector] public int floor;
    private Transform transform;
    private Coroutine coroutine;

    void Start()
    {
        SpawnRange = Contants.SPAWN_RANGE;
        SpawnTime[0] = Contants.SPAWN_TIME[0];
        SpawnTime[1] = Contants.SPAWN_TIME[1];
        transform = Cache.GetTransform(gameObject);
    }

    private IEnumerator Spawn()
    {
        //yield return new WaitForSeconds(Random.Range(SpawnTime[0], SpawnTime[1]));
        float delay = Random.Range(SpawnTime[0], SpawnTime[1]);
        yield return Cache.waitFor(delay);
        if (power_spawned.Count < 3)
        {
            GameObject powerup = Instantiate(PowerUp[Random.Range(0, PowerUp.Length)], transform);
            Cache.GetTransform(powerup).position = new Vector3(Random.Range(-SpawnRange, SpawnRange), 0, Random.Range(-SpawnRange, SpawnRange)) + Contants.FLOOR_POS * (floor-1);
            power_spawned.Add(powerup);
            powerup.GetComponent<PowerBase>().powerSpawn = this;
        }
        StartSpawn();
    }
    
    public void StartSpawn()
    {
        coroutine = StartCoroutine(Spawn());
    }

    public void StopSpawn()
    {
        StopCoroutine(coroutine);
    }

}
