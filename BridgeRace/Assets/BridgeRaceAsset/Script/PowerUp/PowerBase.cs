using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerBase : MonoBehaviour
{
    [HideInInspector] public float Timee;
    [HideInInspector] public PowerSpawn powerSpawn; 
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        var playerhit = Cache.GetHitPowerup(other);
        PowEffect(playerhit);
        Destroy(gameObject);
    }

    protected virtual void PowEffect(HitPowerup playerhit)
    {
        powerSpawn.power_spawned.Remove(gameObject);
    }


}
