using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorControl : MonoBehaviour
{
    // Start is called before the first frame update
    public BrickSpawn brickspawn;
    public PowerSpawn powerspawn;
    public int floor;
    public BridgeControl[] bridges;
    public int bridge_num;
    public float up_floor;

    public void Awake()
    {
        brickspawn.floor = floor;
        powerspawn.floor = floor;
        bridge_num = bridges.Length;
    }
}
