using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickPooler : MonoBehaviour
{
    [System.Serializable]
    public class pool {
        public ColorHandler.Color tag;
        public GameObject brick;
        [HideInInspector] public int size = Contants.POOL_LEN;
    }

    // public ColorHandler.Color color;
    public Dictionary<ColorHandler.Color, Queue<GameObject>> poolDictionary;
    public List<pool> pools;
    public BrickSpawn brickSpawn;
    public bool canSpawn;

    void Start()
    {
        poolDictionary = new Dictionary<ColorHandler.Color, Queue<GameObject>>();
        for (int i = 0; i < Contants.PLAYER_NUM; ++i) {
            pool BrickPool = pools[i];
            Queue<GameObject> BrickObject = new Queue<GameObject>();
            for (int j = 0; j < BrickPool.size; ++j) {
                GameObject obj = Instantiate(BrickPool.brick);
                if (canSpawn) brickSpawn.brick_list[(int)BrickPool.tag].Add(obj);
                obj.SetActive(false);
                Cache.GetTransform(obj).parent = transform;
                BrickObject.Enqueue(obj);
            }

            if (!poolDictionary.ContainsKey(BrickPool.tag))
            {
                poolDictionary.Add(BrickPool.tag, BrickObject);
            }
    }
    }

    public GameObject SpawnFromPool(ColorHandler.Color tag, Vector3 position)
    {
        GameObject obj = poolDictionary[tag].Dequeue();

        obj.SetActive(true);
        Cache.GetTransform(obj).position = position;
        Cache.GetTransform(obj).rotation = Contants.BRICK_ROTATION;

        poolDictionary[tag].Enqueue(obj);

        return obj;
    }


}
