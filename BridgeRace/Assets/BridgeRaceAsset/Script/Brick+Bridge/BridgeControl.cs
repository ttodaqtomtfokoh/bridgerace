using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeControl : MonoBehaviour
{
    [SerializeField] private PathControl Path;

    [HideInInspector] public int brick_placed;
    [HideInInspector] public int[] blockpos = { 0, 0, 0, 0 };

    private BrickPooler pool;
    private Vector3 position;
    private Vector3 block_pos;
    private GameObject[] brick_list = { null, null, null, null, null, null, null, null, null };
    private bool[] reset = { false, false, false, false };

    public Transform[] blockmove;
    public GameObject block_end;
    
    void Start()
    {
        position = transform.position;
        pool = GetComponent<BrickPooler>();
        brick_placed = 0;
    }

    public void PlaceBrick(ColorHandler.Color color)
    {
        if (brick_placed <= Contants.MAX_BRICK_PLACED)
        {
            int tmp = (int)color;
            if (blockpos[tmp] >= brick_placed && brick_placed < Contants.MAX_BRICK_PLACED)
            {
                Vector3 pos = transform.position + Contants.BRICK_POS * blockpos[tmp] + Contants.PLACE_BRICK;
                GameObject obj = pool.SpawnFromPool(color, pos);
                brick_list[brick_placed] = obj;
                brick_placed += 1;
                blockpos[tmp] += 1;
                block_pos = GetBlockPos(blockpos[tmp]);
                blockmove[tmp].position = block_pos;
                if (brick_placed >= Contants.MAX_BRICK_PLACED)
                {
                    blockmove[tmp].gameObject.SetActive(false);
                    block_end.SetActive(false);
                }
            }
            else
            {
                GameObject ToRemove = brick_list[blockpos[tmp]];
                RemoveFromBridge(Cache.GetBlockColor(ToRemove).blockcolor, ToRemove);
                Vector3 pos = new Vector3(position.x, position.y - 1.6f + 0.5f * blockpos[tmp], position.z + 1.25f + blockpos[tmp]);
                GameObject obj = pool.SpawnFromPool(color, pos);
                brick_list[blockpos[tmp]] = obj;
                blockpos[tmp] += 1;
                if (blockpos[tmp] == Contants.MAX_BRICK_PLACED)
                {
                    blockmove[tmp].gameObject.SetActive(false);
                }
                block_pos = GetBlockPos(blockpos[tmp]);
                blockmove[tmp].position = block_pos;
                for (int i = 0; i < Contants.PLAYER_NUM; ++i)
                {
                    if (i != tmp)
                    {
                        reset[i] = true;
                    }
                }
            }
        }
    }

    private Vector3 GetBlockPos(int pos)
    {
        Vector3 value = position + Contants.BLOCK_POS * pos + Contants.PLACE_BLOCK;
        return value;
    }

    private GameObject RemoveFromBridge(ColorHandler.Color tag, GameObject obj)
    {
        pool.poolDictionary[tag].Enqueue(obj);
        obj.SetActive(false);
        return obj;
    }

    private void Reset(int ColorToReset)
    {
        /*yield return Cache.waitFor(0.25f);*/
        block_pos = GetBlockPos(0);
        blockpos[ColorToReset] = 0;
        blockmove[ColorToReset].position = block_pos;
        reset[ColorToReset] = false;
    }

    void Update()
    {
        for (int i = 0; i < Contants.PLAYER_NUM; ++i)
        {
            if (reset[i] && Path.pending_reset[i])
            {
                //StartCoroutine(Reset(i));
                Reset(i);
            }
            //else
            //{
            //    StopCoroutine(Reset(i));
            //}
        }
    }
}
