using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPowerup : MonoBehaviour
{
    private CharacterControl player;
    private Coroutine shield = null;
    private Coroutine speed = null;

    void Start()
    {
        player = Cache.GetCharacterControl(gameObject);
    }

    public void Shield(float EffectTime)
    {
        if (shield != null) StopCoroutine(shield);
        player.shielded = true;
        player.shield_display.SetActive(true);
        shield = StartCoroutine(ShieldOff(EffectTime));
    }

    private IEnumerator ShieldOff(float delay)
    {
        //yield return new WaitForSeconds(delay);
        yield return Cache.waitFor(delay);
        Debug.Log("shield off");
        player.shielded = false;
        player.shield_display.SetActive(false);
        shield = null;
    }

    public void SpeedUp(float EffectTime)
    {
        if (speed != null) StopCoroutine(speed);
        if (!player.speeded)
        {
            player.speed *= Contants.SPEEDUP;
            player.speeded = true;
        }
        player.speed_display.SetActive(true);
        speed = StartCoroutine(SpeedDown(EffectTime));
    }

    private IEnumerator SpeedDown(float delay)
    {
        //yield return new WaitForSeconds(delay);
        yield return Cache.waitFor(delay);
        Debug.Log("speed down");
        player.speed /= Contants.SPEEDUP;
        player.speeded = false;
        player.speed_display.SetActive(false);
        speed = null;
    }
}
