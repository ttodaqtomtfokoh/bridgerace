using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : CharacterControl
{
    [Header("Player")]
    [SerializeField] private FloatingJoystick variableJoystick;


    public void FixedUpdate()
    {
        float z_move = variableJoystick.Vertical * speed;
        float x_move = variableJoystick.Horizontal * speed;
        velocity = rb.velocity.magnitude;
        if (canMove && result == 0)
        {
            Vector3 direction = Vector3.forward * z_move + Vector3.right * x_move;
            if (!on_bridge) direction.y = -9.8f;
            else
            {
                if (variableJoystick.Vertical >= 0)
                {
                    direction.y = -2.8f; 
                }
                else
                {
                    direction.y = -2.8f; 
                    direction.z /= 2;
                }
            }
            rb.velocity = direction;
            Rotation();
        }
    }

    public void Rotation()
    {
        if (variableJoystick.Horizontal != 0 || variableJoystick.Vertical != 0)
        {
            Vector3 moveDir = new Vector3(variableJoystick.Horizontal, 0, variableJoystick.Vertical);
            transform.rotation = Quaternion.LookRotation(moveDir).normalized;
        }
    }

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }
}
