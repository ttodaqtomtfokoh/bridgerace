using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    [SerializeField] private TextMesh countDownText;
    [SerializeField] private GameObject joystick;
    [SerializeField] private GameObject win;
    [SerializeField] private GameObject lose;
    [SerializeField] private CamControl camera;
    
    [SerializeField] private GameObject[] button;
    [SerializeField] private GameObject nextlevel;
    [SerializeField] private GameObject[] map;

    [HideInInspector] public bool GameStarted = false;
    [HideInInspector] public int bot_num;
    [HideInInspector] public List<PowerSpawn> powerup;

    public BotControl[] bots;
    public CharacterControl player;
    public int level;

    private int CountdownTime;
    private int floor_num;
    
    private void Start()
    {
        level = Contants.LEVEL;

        bot_num = Contants.PLAYER_NUM - 1;
        CountdownTime = Contants.COUNTDOWN_TIME;
        StartCoroutine(WaitCountdown());
        GameObject cur_map = Instantiate(map[level - 1]);
        LevelControl cur_level = Cache.GetLevelControl(cur_map);
        floor_num = cur_level.floors.Length;
        cur_level.winControl.gamemanager = this;
        for (int i = 0; i < bot_num; ++i)
        {
            bots[i].win = cur_level.win;
            Array.Resize(ref bots[i].floor, floor_num);
            for (int j = 0; j < floor_num; ++j)
            {
                bots[i].floor[j] = cur_level.floors[j];
            }
        }

        for (int i = 0; i < floor_num; ++i)
        {
            powerup.Add(cur_level.floors[i].powerspawn);
        }
    }

    private IEnumerator WaitCountdown()
    {
        yield return Cache.waitFor(1);
        Countdown();
    }

    private void Countdown()
    {
        if (CountdownTime > 1)
        {
            CountdownTime -= 1;
            countDownText.text = CountdownTime.ToString();
            StartCoroutine(WaitCountdown());
        }
        else
        {
            StartGame();
        }
    }

    private void StartGame()
    {
        GameStarted = true;
        joystick.SetActive(true);
        countDownText.gameObject.SetActive(false);
        for (int i = 0; i < floor_num; ++i) {
            powerup[i].StartSpawn();
        }
        for (int i = 0; i < bot_num; ++i) { 
            bots[i].started = true;
        }
    }

    public void Win()
    {
        Debug.Log("win");
        for (int i = 0; i < floor_num; ++i)
        {
            powerup[i].StopSpawn();
        }
        joystick.SetActive(false);
        StartCoroutine(Active(win, 1f));
        for (int i = 0; i < button.Length; ++i)
        {
            StartCoroutine(Active(button[i], 2.5f));
        }
        if (level < Contants.MAX_LEVEL)
        {
            StartCoroutine(Active(nextlevel, 2.5f));
        }
        player.result = 2;
        for (int i = 0; i < bot_num; ++i)
        {
            bots[i].result = 1;
        }
        camera.Offset = Contants.CAM_END;
        camera.SmoothTime = 0.8f;
    }

    public void Lose(int botwin)
    {
        Debug.Log("lose");
        for (int i = 0; i < floor_num; ++i)
        {
            powerup[i].StopSpawn();
        }
        joystick.SetActive(false);
        StartCoroutine(Active(lose, 1.5f));
        for (int i = 0; i < button.Length; ++i)
        {
            StartCoroutine(Active(button[i], 3f));
        }
        player.result = 1;
        for (int i = 0; i < bot_num; ++i)
        {
            if (i != botwin) bots[i].result = 1;
            else
            {
                bots[i].result = 2;
                camera.target = bots[botwin].transform;
                camera.Offset = Contants.CAM_END;
                camera.SmoothTime = 1;
            }
        }
    }

    private IEnumerator Active(GameObject obj, float delay)
    {
        //yield return new WaitForSeconds(delay);
        yield return Cache.waitFor(delay);
        obj.SetActive(true);
    }
}
