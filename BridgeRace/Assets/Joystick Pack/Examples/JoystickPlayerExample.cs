﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;
    public FloatingJoystick variableJoystick;
    public Rigidbody rb;
    private bool on_bridge = false;

    public void FixedUpdate()
    {
        Vector3 direction = Vector3.forward * variableJoystick.Vertical * speed + Vector3.right * variableJoystick.Horizontal * speed;
        // rb.AddForce(direction * speed * Time.fixedDeltaTime, ForceMode.VelocityChange);
        if (!on_bridge) direction.y = -9.8f;
        else direction.y = -2.8f;
        rb.velocity = direction;
    }

    public void OnBridge()
    {
        on_bridge = true;
    }

    public void OffBridge()
    {
        on_bridge = false;
    }
}